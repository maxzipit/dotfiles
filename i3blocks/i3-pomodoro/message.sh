#! /bin/bash

while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    -m|--message)
    MESSAGE="$2"
    shift # past argument
    shift # past value
    ;;
    
esac
done

XDG_RUNTIME_DIR=/run/user/1000 paplay /usr/share/sounds/freedesktop/stereo/complete.oga
ACTION=$(dunstify -a "i3_pomodoro" --action="clcik,Continue" "$MESSAGE")

if [ $ACTION = "clcik" ]; then
    BLOCK_BUTTON=1 bash -c '/home/maks/.config/i3blocks/i3-pomodoro/i3-pomodoro & > /dev/null;'
fi

exit 0

#!/bin/bash
# Play pause keys for tizonia

set -a

ISBLOCK=0

while getopts ":hb" opt; do
  case ${opt} in
    h )
      echo "Usage:"
      echo "    tizoniaShortcuts.sh -h              Display this help message."
      echo "    tizoniaShortcuts.sh -b              Show i3block."
      echo "    tizoniaShortcuts.sh play_pause      Play/Pause."
      echo "    tizoniaShortcuts.sh stop            Stop."
      exit 0
      ;;
    b )
      ISBLOCK=1
      ;;
   \? )
     echo "Invalid Option: -$OPTARG" 1>&2
     exit 1
     ;;
  esac
done
shift $((OPTIND -1))

subcommand=$1; shift  # Remove 'pip' from the argument list

function get_status {
  local IS_RUNNING=$(pgrep -x tizonia)

  if [[ -z $IS_RUNNING ]]; then
    PSTATUS="NOR"
  else
    PSTATUS=$(tizonia-remote playstatus)
  fi

  echo $PSTATUS
}

function run_rofi {
  rofi -show find -modi find:/home/maks/.config/rofi/scripts/tizoniasearch.sh
}

function print_block {
    local BLOCK=""

    local PSTATUS=$(get_status)

    if [[ $PSTATUS ==  "\"Playing\"" ]]; then
        BLOCK="<span foreground=\"#F2E750\" ></span>"
    elif [[ $PSTATUS == "\"Paused\"" ]]; then
        BLOCK="<span foreground=\"#D9A036\" ></span>"
    elif [[ $PSTATUS == "\"Stoped\"" ]]; then
        BLOCK=""
    fi
 
    echo " $BLOCK"
    echo " $BLOCK"
}

function change_playstate {
    local PSTATUS=$(get_status)

    if [[ $PSTATUS ==  "\"Playing\"" ]]; then
        tizonia-remote pause
    elif [[ $PSTATUS == "\"Paused\"" ]]; then
        tizonia-remote play
    elif [[ $PSTATUS == "\"Stoped\"" ]]; then
        tizonia-remote play
    elif [[ $PSTATUS == "NOR" ]]; then
        run_rofi
    fi
}

function stop {
    local PSTATUS=$(get_status)

    if [[ $PSTATUS == "\"Stoped\"" ]]; then
      killall tizonia
    else
      tizonia-remote stop
      PSTATUS=$(get_status)
      if [[ $PSTATUS != "\"Stoped\"" ]]; then
        killall tizonia
      fi
    fi
}


case "$BLOCK_BUTTON" in
    1) change_playstate ;;
    2) stop ;;
    3) ;;
    4) ;;
    5) ;;
esac

if [[ $ISBLOCK == 1 ]]; then
    print_block
    exit 0
else
    case "$subcommand" in
    # Parse options to the play_pause sub command
      stop )
      echo "stop"
        stop
        ;;
      *)
        echo "change"
        change_playstate
        ;;
    esac
    pkill -SIGRTMIN+2 i3blocks
    exit 0
fi

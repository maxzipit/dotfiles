#!/usr/bin/env bash

#USE: rofi  -show find -modi find:path_to_this_script
#SEARCH=$(curl -sG 'https://suggestqueries.google.com/complete/search?client=firefox&ds=yt' --data-urlencode "q=$Q" | jq --raw-output '.[1][]')
if [ ! -z "$@" ]
then
  QUERY=$@

  if [[ "$@" == \!\!* ]]
  then
    echo "!!-- Type your search query for YouTube"
    echo "!!-- You can print this help by typing !!"
  else
    coproc ( tizonia -d --youtube-api-key $KEY --youtube-audio-search "$QUERY"  > /dev/null 2>&1 )
    exec 1>&-
    exit;
  fi
else
  echo "!!-- Type your search query for YouTube"
  echo "!!-- You can print this help by typing !!"
fi
